import java.util.Scanner;


public class Schleifen2 {
	
	static String matrix[][] = new String[10][10];
	
	public static void main(String[] args) {
		
		
		//Zahl einlesen
		Scanner scanner = new Scanner(System.in);
		System.out.print("Bitte Zahl zwischen 2 und 9 eingeben:");
		int zahl = scanner.nextInt();
		
		//Zahl auf Gültigkeit prüfen
		if(zahl < 2 || zahl > 9)
			return;
		
		
		//matrix füllen
		int zaehlen = 0;
		for(int i = 0; i < 10; i++) {
			for(int j = 0; j < 10; j++) {
				matrix[i][j] = Integer.toString(zaehlen);
				zaehlen++;
			}
		}
		
		//matrix bearbeiten
		for(int i = 0; i < 10; i++) {
			for(int j = 0; j < 10; j++) {
				
				//0 soll nicht ersetzt werden
				if(i == 0 && j == 0)
					j++;
				
				if(Integer.parseInt(matrix[i][j]) % zahl == 0)
					matrix[i][j] = "*";
				

				else if(berechneQuersumme(Integer.parseInt(matrix[i][j])) == zahl)
					matrix[i][j] = "*";
				
				else if(enthaeltZiffer(Integer.parseInt(matrix[i][j]), zahl) == true)
					matrix[i][j] = "*";
						
			}
		}
		
		
		//matrix ausgeben
		for(int i = 0; i < 10; i++) {
			for(int j = 0; j < 10; j++) {
				System.out.print(matrix[i][j] + "  ");
			}
			
			System.out.println();
		}
		
		
		scanner.close();
	}
	
	
	public static int berechneQuersumme(int zahl) {
		int summe = 0;
		
		for(int i = 0; i < berechneAnzahlZiffern(zahl); i++) {
			summe += getZiffer(zahl, i);
		}
		
		
		return summe;
	}
	
	
	public static int berechneAnzahlZiffern(int zahl) {
		String str = Integer.toString(zahl);
		return str.length();
	}
	
	public static boolean enthaeltZiffer(int zahl, int ziffer) {
		
		for(int i = 0; i < berechneAnzahlZiffern(zahl); i++) {
			if(getZiffer(zahl, i) == ziffer)
				return true;
		}
		
		return false;
	}
	
	public static int getZiffer(int zahl, int ziffer) {
		String str = Integer.toString(zahl);
		
		String zifferChar = str.substring(ziffer, ziffer+1);
		
		return Integer.parseInt(zifferChar);
	}

}
