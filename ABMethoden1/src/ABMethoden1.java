
public class ABMethoden1 {
	
	public static enum form{
		WUERFEL, QUADER, PYRAMIDE, KUGEL
	}

	public static void main(String[] args) {
		
	}
	
	public static double multiplizieren(double wert1, double wert2) {
		return wert1 * wert2;
	}
	
	public static double berechneVolumen(form typ, int...x) {
		switch(typ) {
		
		case WUERFEL : return berechneVolumenWuerfel(x[0]);
		
		case QUADER : return berechneVolumenQuader(x[0], x[1], x[2]);
		
		case PYRAMIDE : return berechneVolumenPyramide(x[0], x[1]);
		
		case KUGEL : return berechneVolumenKugel(x[0]);
		
		default: return -1;		
		}
	}
	
	public static double berechneVolumenWuerfel(double a) {
		return a * a * a;
	}
	
	public static double berechneVolumenQuader(int a, int b, int c) {
		return a * b * c;
	}
	
	public static double berechneVolumenPyramide(int a, int h) {
		return a * a * h / 3.0;
	}
	
	public static double berechneVolumenKugel(double r) {
		return (4/3) * r*r*r * 3.146;
	}
	
	
}
