import java.util.Scanner;


public class Schleifen1 {
	

	public static void main(String[] args) {
		
		int b = 3;
		int h = 4;
		
		for(int i = 1; i <= h; i++) {
			
			for(int j = 1; j <= (h-i)*b; j++)
				System.out.print(" ");
			
			for(int j = 1; j <= i*b; j++)
				System.out.print("*");
			
			System.out.println();
			
		}
	}
}
