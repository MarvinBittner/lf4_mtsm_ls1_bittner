/*   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author Marvin Bittner
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne = 300000000000l;    
    // Wie viele Einwohner hat Berlin?
     int bewohnerBerlin = 3645000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
     short alterTage = 23 * 365; 
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
      int gewichtKilogramm =  150000;  
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
       int flaecheGroessteLand = 17100000;
    
    // Wie groß ist das kleinste Land der Erde?
    
       double flaecheKleinsteLand = 0.44;
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Bewohner Berlin: " + bewohnerBerlin);
    
    System.out.println("Alter in Tagen: " + alterTage);
    
    System.out.println("Gewicht schwerstes Tier: " + gewichtKilogramm + " kg");
    
    System.out.println("Fläche größtes Land: " +  flaecheGroessteLand + " km²");
    
    System.out.println("Fläche kleinstes Land: " + flaecheKleinsteLand + " km²");    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
