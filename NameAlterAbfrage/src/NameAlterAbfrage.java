import java.util.Scanner;


public class NameAlterAbfrage {

	public static void main(String[] args) {
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Guten Tag!");
		
		System.out.print("Bitte Namen eingeben: ");
		String name = userInput.next();
		
		System.out.print("Bitte Alter eingeben:");
		int alter = userInput.nextInt();
		
		
		System.out.println("Name: " + name);
		System.out.println("Alter: " + alter);
		
		userInput.close();
	}

}