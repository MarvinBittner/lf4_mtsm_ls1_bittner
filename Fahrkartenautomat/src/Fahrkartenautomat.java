import java.util.Scanner;


class Fahrkartenautomat
{
	
	
	private static double[] preise = {
			2.9,
			3.3,
			3.6,
			1.9,
			8.6,
			9.0,
			9.6,
			23.5,
			24.3,
			24.9
	};
	
	
	private static String[] bezeichnungen = {
			"Einzelfahrschein Regeltarif AB [2,90 EUR] (1)",
			"Tageskarte Regeltarif AB [8,60 EUR] (2)",
			"Einzelfahrschein Berlin ABC (3)",
			"Kurzstrecke [1,90 EUR] (4)",
			"Tageskarte Berlin AB [8,60 EUR] (5)",
			"Tageskarte Berlin BC [9,00 EUR] (6)",
			"Tageskarte Berlin ABC [9,60 EUR] (7)",
			"Kleingruppen-Tageskarte Berlin AB [23,50 EUR] (8)",
			"Kleingruppen-Tageskarte Berlin BC [24,30 EUR] (9)",
			"Kleingruppen-Tageskarte Berlin ABC [24,90 EUR] (10)"
	};
	
	
	
	
	public static Scanner scan = new Scanner(System.in);
	
	
	
    public static void main(String[] args)
    {
    	
    	System.out.println("Fahrkartenbestellvorgang:");
    	for(int i = 0; i < 25; i++)
    		System.out.print("=");
    	
    	for(int i = 0; i < 3; i++)
    		System.out.println();
    	
    	
    	while(true) {
    		double zuZahlen = fahrkartenBestellungErfassen();
        	
        	double rueckgeld = fahrkartenBezahlen(zuZahlen);
        	
        	fahrkartenAusgeben();
        	
        	rueckgeldAusgeben(rueckgeld);
    	}
    	    	
    }
    
    
    public static double fahrkartenBestellungErfassen() {
    	double zuZahlen = 0;
    	
    	
    	System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus:");
    	
    	
    	for(int i = 0; i < bezeichnungen.length; i++) {
    		
    		System.out.print("              ");
    		System.out.println(bezeichnungen[i]);
    		
    	}
    	
    	
    	int auswahl = 0;
    	
    	do {
    		
    		System.out.print("Ihre Wahl: ");
    		auswahl = scan.nextInt();
    		
    		if(auswahl < 1 || auswahl > 3)
    			System.out.println("  >>Falsche Eingabe<<");
    			
    		
    	}while(auswahl < 1 || auswahl > preise.length);	
    	   	
    	
    	zuZahlen = preise[auswahl-1];
    	
    	return zuZahlen;    	
    }
    
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze = 0.0;
    	
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = scan.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
    	
    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
        	warte(250);
         }
         System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
    	
    	if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", rueckgabebetrag );
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
            	muenzeAusgeben(2, "EURO");
            	rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
            	muenzeAusgeben(1, "EURO");
            	rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
            	muenzeAusgeben(50, "CENT");
            	rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
            	muenzeAusgeben(20, "CENT");
  	          	rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
            	muenzeAusgeben(10, "CENT");
            	rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
            	muenzeAusgeben(5, "CENT");
  	          	rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
        
        
        for(int i = 0; i < 3;  i++)
        	System.out.println();
        
    }
    
    public static void warte(int millisekunde) {
    	try {
    		Thread.sleep(millisekunde);
    	}catch (InterruptedException e) {
    		// TODO Auto-generated catch block
			e.printStackTrace();
    	}
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.printf("%d %s\n", betrag, einheit);
    }
    
}